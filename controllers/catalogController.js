
const Catalog = require('../models/catalog');
exports.index = async (req,res,next) => {
    try {
        const catalogs = await Catalog.find({});
        res.send(catalogs);
    } catch(err){

    }
}
exports.store = async (req,res,next) => {
    try {
        let catalog = new Catalog();
        catalog.name = req.body.name;
        catalog.description = req.body.description;
        catalog.status = req.body.status;
        catalog = await catalog.save();
        res.send(catalog);
    } catch(err){

    }
}
exports.update = async (req,res,next) => {
    try {
        const id = req.params.id;
        let catalog = await Catalog.findById(id);
        catalog.name = req.body.name;
        catalog.description = req.body.description;
        catalog.status = req.body.status;
        catalog = await catalog.save();
        res.send(catalog);
    } catch(err){
        next(err);
    }
}
exports.delete = async (req,res,next) => {
    try {
        const id = req.params.id;
        let catalog = await Catalog.findById(id);
        if(catalog){
            await catalog.delete();
            res.send({message: 'Catalog Successfully Deleted!'});
        } else {
            res.send({message: 'Catalog Not Found!'});
        }
    } catch(err){
        next(err);
    }
}