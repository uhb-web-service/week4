const catalogRoute = require('./catalog');

module.exports=(app)=>{
    app.use('/api/catalog',catalogRoute);
}