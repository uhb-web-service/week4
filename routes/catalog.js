const express = require("express");
const router = express.Router();
const catalogController = require('../controllers/catalogController');
router.get('/',catalogController.index);
router.post('/store',catalogController.store);
router.patch('/:id',catalogController.update);
router.delete('/:id',catalogController.delete);
module.exports = router;