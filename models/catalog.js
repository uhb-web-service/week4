const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CatalogSchema = new Schema({
    name: { type: String, required: true},
    description: { type: String, required: false},
    status : { type: String, required: false,default: "draft"},
    createdAt: { type: Date, default: Date.now()},
},{
    versionKey: false // You should be aware of the outcome after set to false
});

module.exports = mongoose.model('catalog',CatalogSchema);