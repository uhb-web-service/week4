const http = require('http');
const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const config = require('./config');
const routes = require('./routes');
app.use(express.json({limit: '50mb'}));
mongoose.Promise = global.Promise;
mongoose.connect(config.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true});
app.use(express.json({limit: '50mb'}));

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname,"public")));
routes(app,null);
http.createServer(app.handle.bind(app)).listen(config.port,()=>{
    console.log("Listening...on port "+config.port);
});